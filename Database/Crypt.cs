﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;

namespace WebApplication1.Database
{
    public abstract class Crypt
    {
        public static string passwordEncypter (string pass)
        {   // im not doing Salting because im lazy
            //gets bytes from string
            var data = Encoding.ASCII.GetBytes(pass);

            // hashes bytes
            var sha1 = new SHA1CryptoServiceProvider();
            var sha1data = sha1.ComputeHash(data);

            // turns back into string
            string hashedPassword = Encoding.ASCII.GetString(sha1data);

            return hashedPassword;
        }

        public static readonly RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
        // generates a random string
        public static string genRand()
        {
            byte[] bytes = new byte[6];
            rngCsp.GetBytes(bytes);
            string strBytes = Convert.ToBase64String(bytes);
            string strBytesRep = strBytes.Replace('/', '?');
            
            return strBytesRep;
        }
    }
}