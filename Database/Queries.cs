﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace WebApplication1.Database
{
    // holds methods for querying the database
    public abstract class Queries
    {

        // checks if a record is in the database by returning true/false
        public static bool checkIfRecordInDatabase(SqlCommand query)
        {

            // Estalish the connection
            SqlConnection con = Connection.databaseConnect();
            query.Connection = con;

            // see how many results of query (in int32) by .ExecuteScalar()
            var result = query.ExecuteScalar();

            // close connection
            Connection.databaseDisconnect(con);

            if (Convert.ToInt32(result) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static void execCommand(SqlCommand query)
        {
            // Estalish the connection and exec the query
            SqlConnection con = Connection.databaseConnect();
            query.Connection = con;

            query.ExecuteNonQuery();

            Connection.databaseDisconnect(con);
        }

        // takes in a string of the query and a dictionary with keys representing params to be substituted and their values representing
        // the params with which they will be substituted with
        // sets up the parameterised query and returns an SQLCommand object 
        public static SqlCommand makeQuery(string queryString, IDictionary<string, string> parameters)
        {
            SqlCommand Query = new SqlCommand(queryString);

            foreach (string key in parameters.Keys )
            {
                Query.Parameters.AddWithValue(key, parameters[key]);
            }

            return Query;
        }

    }
}