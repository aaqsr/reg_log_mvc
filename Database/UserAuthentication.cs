﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using WebApplication1.Database;

namespace WebApplication1.Database.UsersData
{
    // Holds methods called by the User namespace to authenticate the user, by calling the Queries class in the database namespace
    public abstract class UserAuthentication
    {
        public static void addUserInDatabase(string user, string pass)
        {
            // first encrypt the password
            string encryptedPass = Crypt.passwordEncypter(pass);

            string query = "INSERT INTO Awab_TestDB.dbo.userdata (username, pincode) VALUES(@user, @pass)";

            var parameters = new Dictionary<string, string>()
            {
                { "user", user },
                { "pass", encryptedPass }
            };

            Queries.execCommand( Queries.makeQuery(query, parameters) );
        }

        // overloads the previous method and checks a username and password pair 
        // checks if user pass pair is in the database
        public static bool checkUserInDatabase(string user, string pass)
        {
            // first encrypt the password
            string encryptedPass = Crypt.passwordEncypter(pass);

            string query = "SELECT COUNT(*)FROM userdata WHERE username = @user AND pincode = @pass";

            var parameters = new Dictionary<string, string>()
            {
                { "user", user },
                { "pass", encryptedPass }
            };

            return Queries.checkIfRecordInDatabase( Queries.makeQuery(query, parameters) );
        }

        // checks if just username is in database
        public static bool checkUserInDatabase(string user)
        {
            string query = "SELECT COUNT(*)FROM userdata WHERE username=@user";

            var parameters = new Dictionary<string, string>()
            {
                { "user", user },
            };

            return Queries.checkIfRecordInDatabase(Queries.makeQuery(query, parameters));
        }

    }

}