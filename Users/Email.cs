﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WebApplication1.Users
{
    public abstract class Email
    {
        // checks if email is valid
        public static bool isEmailValid(string email)
        {
            // sets regex string to match email in the form (a.)b@xyz.com
            Regex rx = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\b");
            // gets matches
            MatchCollection matches = rx.Matches(email);

            if (matches.Count == 1)
            {
                return true;
            }

            return false;
        }

        public static void sendEmail(string body, string email) // in future also needs to include email object
        {

        }
    }
}
